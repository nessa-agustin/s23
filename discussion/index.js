// console.log('test')

/* 
    Objects 
        - an object is a data type that is used to represent real world objects, it is also a collection of related data and/or functionalities.

        Creating Objects using object literals
        -syntax:
            let objectName = {
                keyA: valueA,
                keyB: valueB,
                keyC: valueC,
            }
*/

let student = {
    firstName : 'Rupert',
    lastName : 'Ramos',
    age: 30,
    studentId: '2022-009752',
    email: ['rupert.ramos@mail.com','rbr209@gmail.com'],
    address: {
        street: '125 Ilang-ilang St',
        city: 'Quezon City',
        country: 'Philippines'
        }
}

console.log(student);

/* 
    Creating Objects using constructor Function
        Create a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

            Syntax:
                function objectName(valueA, valueB){
                    this.keyA = valueA
                    this.keyB = valueB
                }

                let var = new function objectName(valueA, valueB)

                -"this" is a keyword that is used for invoking, it refers to the global object
                - don't forget to add "new" keyword when creating variables.
*/
// we use Pascal casing for the objectName when creating objects using constructor function
function Laptop(name, mfgDate){
    this.name = name
    this.mfgDate = mfgDate
}

let laptop = new Laptop('Lenovo', 2008);
console.log('Result of creating objects using object constructor: ');
console.log(laptop);


let mylaptop = new Laptop('Macbook Air', 2020);
console.log('Result of creating objects using object constructor: ');
console.log(mylaptop);


// Creating empty object as placeholder

let computer = {};
let myComputer = new Object();

myComputer = {
    name: 'Asus',
    mfgDate: 2012
}

console.log(myComputer);



function MobilePhone(brand, model, os){
    this.brand = brand
    this.model = model
    this.os = os
}


let phoneA = new MobilePhone('Apple','iPhone13','iOS');
let phoneB = new MobilePhone('Samsung','Galaxy Fold','Android');

console.log(phoneA)
console.log(phoneB)

// Accessing Object Property

// using the dot notation
/* 
    Syntax:
        objectName.property
*/
console.log('Result from dot notation: ' + mylaptop.name)

// Using the bracket notation
// Syntax: objectName["name"]

console.log('Result from dot notation: ' + mylaptop["name"])

// Accessing array objects
let array = [laptop, mylaptop];
// let array = [
//     {name: "Lenovo",mfgDate: 2008},
//     {name: "Macbook Air",mfgDate: [2019, 2020]},
    
// ];

// Dot Notation
console.log(array[0].name)

// Square bracket notation
console.log(array[0]['name'])

// Initializing /Adding /Deleting /Reassigning object properties

let car = {};
console.log(car);

// Adding object properties

car.name = 'Honda Civic';
console.log('Result from adding property using dot notation:')
console.log(car);

car['mfgDate'] = 2009;
console.log(car);

car.name = ['Ferrari', 'Toyota']
console.log(car);


// Deleting object properties

// delete car['mfgDate']
car['mfgDate'] = "";
console.log('Result after delete:')
console.log(car);

// Reassigning object properties
car.name = 'Tesla'
console.log('Result after reassign:')
console.log(car);


/* 
    Object Method
        - a method where a function serves as a value in a property. They are also functions and one of the key difference they have is that methods are functions related to a specific object property.

*/

let person = {
    name: 'John',
    lname: 'Smith',
    talk: function(){
        // console.log('Hello my name is ' + this.name);
        return this.name + ' ' + this.lname;
    }
}

console.log(person)
console.log('Result from object methods: ')
person.talk()
console.log(person.talk())


person.walk = function(){

    console.log(this.name + ' walked 25 steps forward')
}

person.walk()

let friend = {
    firstName : 'John',
    lastName : 'Doe',
    address: {
        city: 'Austin, Texas',
        country: 'US'
    },
    emails: ["johnD@email.com","joel2@yahoo.com"],
    introduce: function(){
        console.log('My name is ' + this.lastName)
    }
}


let myPokemon = {
    name: 'Pikachu',
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This pokemon tackled targetPokemon")
        console.log("targetPOkemon's health is now reduced to targetPOkemonshealth")
    },
    faint: function(){
        console.log("Pokemon fainted")
    }
}

console.log(myPokemon)

// Using object constructor

function Pokemon(name,level){
    // Properties
    this.name = name
    this.level = level
    this.health = 3 * this.level
    this.attack = 2 * this.level

    // Methods
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name)
        target.health = target.health - this.attack
        console.log(target.name + "'s health is now reduced " + (target.health - this.attack))

        if(target.health <= 5){
            target.faint()
        }
    },
    this.faint = function(){
        console.log(this.name + ' fainted')
    }


}


let charizard = new Pokemon('Charizard',12)
let squirtle = new Pokemon('Squirtle',6)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)


//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.


Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:

console.log("")
console.log("")



let pidgeotto = new Pokemon('Pidgeotto',15)
let jigglypuff = new Pokemon('Jigglypuff',4)
let psyduck = new Pokemon('Psyduck',13)


let trainer = {
    name: 'Misty',
    age: 10,
    pokemon: [pidgeotto.name,jigglypuff.name,psyduck.name],
    friends: {
        hoenn: ['Sam',"Rick"],
        kanto: ['Morty','Deedee']
    },
    talk: function(pokemonName){
        console.log(pokemonName + ', I choose you');
    }

}

console.log(trainer);

console.log('Result of dot notation:')
console.log(trainer.name)

console.log('Result of bracket notation:')
console.log(trainer['pokemon'])

console.log('Result of talk method:')
trainer.talk(jigglypuff.name)


jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)
jigglypuff.tackle(pidgeotto)




